###########################
Computational Thinking (CT)
###########################

Computational Thinking (CT) mulai dikenal di Indonesia pada tahun 2016, pada saat
Indonesia pertama kalinya mulai berpartisipasi mengadakan Bebras Challenge. CT adalah
kemampuan menyelesaikan suatu masalah yang kompleks dengan cara memahami permasalahan
dan mengembangkan solusi yang ada kemudian menyajikannya dengan cara yang dapat dipahami
oleh komputer, manusia, atau dipahami oleh keduanya. Menurut Barefoot Computing, CT terdiri
dari enam konsep yang berbeda, yaitu : Logic, Algorithms, Decomposition, Patterns, Abstraction,
dan Evaluation.

Untuk melatih kemampuan CT, salah satu cara yang dapat dilakukan adalah memberikan
latihan soal yang berkaitan dengan Computational Thinking (CT). Soal CT biasanya ditujukan
untuk kategori umur atau jenjang pendidikan tertentu dan setiap negara memiliki standar
kategori umur yang berbeda, contohnya di Indonesia untuk usia 6 - 18 tahun terdapat tiga
kategori umur yaitu SD, SMP, dan SMA.

Soal-soal CT yang ada saat ini kebanyakan berasal dari negara-negara maju, dan belum
banyak soal CT yang berasal dari Indonesia maupun yang berbahasa Indonesia. Teknik Informatika UNPAR saat ini sedang mensosialisasikan Computational Thinking kepada masyarakat luas.
Namun, Teknik Informatika UNPAR masih merujuk ke website-website lain sebagai referensi untuk mensosialisasikan Computational Thinking. Oleh karena itu, diperlukan sebuah website yang
dapat membantu Teknik Informatika UNPAR untuk mensosialisasikan Computational Thinking
kepada masyarakat luas dan dapat pula dijadikan referensi dalam sosialisasi Computational
Thinking.

Penelitian ini dilakukan untuk membuat sistem informasi berbasis web yang dapat menampung soal-soal CT dan juga pembahasannya. Sistem informasi ini juga dapat melakukan
pencarian soal berdasarkan konsep-konsep CT, tipe soal, jenjang pendidikan, dan berdasarkan
umur. Selain melakukan pecarian soal, sistem informasi ini dapat menampilkan halaman penjelasan CT dan konsep-konsepnya, serta kegiatan sosialisasi CT yang telah dilakukan oleh Teknik
Informatika UNPAR.

Sistem informasi ini dibangun dengan menggunakan bahasa pemrograman PHP dan menggunakan framework CodeIgniter. Berdasarkan hasil pengujian, dapat disimpulkan bahwa sistem
informasi yang dibuat berhasil dibangun sesuai kebutuhan pengguna.