<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>

	<style>
	            .tipe_gambar{
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }

						.alert-info{
                margin-left:0px;
                margin-right:10px;
            }

						.demoInputBox {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                
            }

						.keteranganGambar {
                padding: 3px 5px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }
	</style>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

			<!--breadcrumbs disini-->

			<!--Bagian session-->
				<?php if ($this->session->flashdata('berhasil_tambah_gambar')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('berhasil_tambah_gambar'); ?>
				</div>
				<?php elseif ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_tipe_file_salah')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_tipe_file_salah'); ?>
				</div>
                <?php elseif ($this->session->flashdata('gagal_file_terlalu_besar')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_file_terlalu_besar'); ?>
				</div>
				<?php endif; ?>

				<br>	
				<h5 class="font-weight-bold text-center">Sunting Data Liputan CT</h5>
				<br>
				
				<form action="<?php base_url('admin/liputan_ct/edit') ?>" method="post" id="form_liputan" enctype="multipart/form-data">

				<!-- Card  -->
				<div class="card mb-3">	
					<div class="card-header">

					<a href="<?php echo site_url('admin/liputan_ct/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>

					<div class="card-header">
                        <h5 class="text-center">Soal CT</h5>
                    </div>


					<div class="card-body">

						   <input type="hidden" name="id_liputan" value="<?php echo $liputan_ct->id_liputan?>" />

                           <div class="form-group">
								<label for="judul_liputan">Judul Liputan*</label>
								<input class="form-control 
								<?php echo form_error('judul_liputan') ? 'is-invalid':'' ?>"
								 type="text" name="judul_liputan" placeholder="Judul Liputan" value="<?php echo htmlspecialchars($liputan_ct->judul_liputan)?>" />
								<div class="invalid-feedback">
									<?php echo form_error('judul_liputan') ?>
								</div>
							</div>							

							<div class="form-group">
								<label for="deskripsi_liputan">Deskripsi Liputan*</label>
								<textarea class="form-control 
								<?php echo form_error('deskripsi_liputan') ? 'is-invalid':'' ?>"
								 type="text" name="deskripsi_liputan" id="deskripsi_liputan" placeholder="Deskripsi Liputan" rows="10" cols="80" />
								 <?php echo $liputan_ct->deskripsi_liputan?>
								 </textarea>
								 <script type="text/javascript">
                                            CKEDITOR.replace( 'deskripsi_liputan',{
                                            enterMode: CKEDITOR.ENTER_BR
                                            });
   								</script>
								<div class="invalid-feedback">
									<?php echo form_error('deskripsi_liputan') ?>
								</div>
							</div>

							<div class="form-group">
							<label for="tanggal">Tanggal*</label>
						    <input class="form-control 
							<?php echo form_error('tanggal') ? 'is-invalid':'' ?>" type="date" name="tanggal" value="<?php echo $liputan_ct->tanggal?>">
							<div class="invalid-feedback">
								<?php echo form_error('tanggal') ?>
							</div>
							</div>

                            <?php foreach ($data_imageLiputan as $i)	{					
							 $id_image = $i->id_image;
                            if($id_image != null){
									echo "Gambar Liputan :  ";
								
									echo '<a id="delete_liputan" style="float:right" class="text-danger delete3" href="#">  Delete Gambar <i class="fas fa-times text-danger"></i></a>';	

                                    echo '<div style="width:auto; height:24rem; display:flex; justify-content:center; align-items:center;">
                                    <img style="width:auto;height:18rem" src="data:image/jpeg;base64,'.$this->controller->display_gambar_liputan($id_image).'"/>
                                    </div>';
                                    echo '<p class="text-center"> Keterangan Gambar : '.$i->keterangan_gambar.'</p>';
                                    echo '<input type="hidden" name="id_image[]" value="'.$id_image.'" />
                                    <label>Sunting Gambar : </label>
                                    <div class="alert alert-info row">             									
                                            <div class="col">
                                                <label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label><span id="jawaban-error"></span>	
                                                <br>										 
                                                <input type="file" style="height:66%" accept="image/*" name="multipleFiles[]" >
                                            </div>
                                            <div class="col"> 
                                                    <label style="margin-left:17px">Keterangan Gambar : </label> <br>
                                                    <input type="text" name="keteranganGambar[]" style="margin-left:17px;width:500px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>
                                            </div>
										</div> ';
										
										echo '<a id="link4" class="link_liputan" type="hidden" onclick="window.location.href=\'';
										echo site_url('admin/liputan_ct/delete_image_liputan/'.$id_image);
										echo '\'"></a> ';
									}
								}
                            ?>

                            <div class="form-group" >
									<div id="uploadFileContainer2" ></div>
									<div>
										<button id="ADDFILE2" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar</button>    
									</div>
							</div>

                            <div class="text-center" style="margin-top:50px ">
								<input class="btn btn-primary" type="submit" name="btn" value="Save" />
							</div>
							

						</form>
						

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php //$this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>

$(document).ready(function(){
	function deleteConfirm(url){
		$('#btn-delete').attr('href',url);
		$('#deleteModal').modal();
		}

	$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});

	$(document).on('click','button#ADDFILE2', function(event){
				event.preventDefault();
				addFileInput2();
		});                 

		function addFileInput2() {
			var html = '';
            html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label><br>';
            html+='<input type="file" accept="image/*" name="multipleFiles2[]">';
            html+='</div>';   
            html+='<div class="col">'; 
            html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
            html+='<input type="text" name="keteranganGambar2[]" style="margin-left:17px;width:400px" id="keteranganGambar2" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
            html+='</div class="col">';
            html+='<div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
            html+='</div>';
			$("div#uploadFileContainer2").append(html);
		}


    //delete gambar liputan
	var n = 0;
	$('.delete3').each(function() {
		n++;
		$(this).attr("id", "liputan"+n);
	});

	var m = 0;
		$('.link_liputan').each(function() {
			m++;
			$(this).attr("name", "liputan"+m);
		});

	$(".delete3").click(function() {
		var id_link3 = this.id;

			$('.link_liputan').each(function() {
				var val3 = this.name;

				if(val3 == id_link3){
						$('a[name="' + val3 + '"]').click();
						$("#form_liputan").submit();	
					}

				//alert(val3);
			});

			//alert(id_link3);
	});
			

});



</script>


</html>