<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<body>

   
  
    <div class="card" style="padding-bottom:30px">
          
        <div class="card-header">
		   <a href="<?php echo site_url('admin/liputan_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
		</div>

          <div class="card-header">
            <h5 class="text-center">Liputan CT</h5>
          </div>
          
          <div class="card-body" style="padding-left:50px;padding-right:50px">
             
            <p class="card-text text-center"><label>Judul Liputan : </label> <?php echo $data_liputan->judul_liputan?></p>

            <?php
                  foreach ($data_imageLiputan as $j){
                  if($j->id_image != null){
                    echo '<img class="card-img img-responsive center-block" style="width:auto;height:40rem" src="data:image/jpeg;base64,'.$this->controller->display_gambar_liputan($j->id_image).'"/>';
                    echo '<p class="text-center">'.$j->keterangan_gambar.'</p>';
                  } 
                }

                echo '<br>';
                $tgl = $data_liputan->tanggal;
                $res = $this->controller->tanggal_indo(date('Y-m-d', strtotime($tgl)));
                echo '<p class="text-center">('.$res.') </p>';
              ?>



            <label>Deskripsi Liputan : </label><p class="card-text"><?php echo $data_liputan->deskripsi_liputan?></p>



          </div>

    </div>


</body>

<script>
$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
</script>


