<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">



	<?php $this->load->view("admin/_partials/navbar.php") ?>

	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">
			
			<?php if($this->session->flashdata('success_login')){ ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Berhasil login,</strong> <?php echo $this->session->flashdata('success_login'); ?>
				</div>

			<?php } else if($this->session->flashdata('error')){  ?>

				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Tidak berhasil login, </strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>



				<!--Breadcrumb Simpan disini-->
				<br>
				<h5 class="text-center text-bold font-weight-bold">Daftar Admin</h5>
				<br>
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/admin/add')?>"><i class="fas fa-plus"></i> Tambah Admin</a>
					</div>
					<div class="card-body">

					<?php if (!empty($data_admin)): ?>

						<div class="table-responsive">
							<table class="table table-bordered table-hover" id="table_admin" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Username</th>
									</tr>
								</thead>
								<tbody>

									<?php foreach ($data_admin as $i): ?>
									<tr>
										<td>
											<?php echo $i->nama?>
										</td>
										<td>
											<?php echo $i->username?>
										</td>
										<td width="250">
											<a href="<?php echo site_url('admin/admin/edit/'.$i->id_admin) ?>"
								            class="btn btn-small text-warning"><i class="fas fa-edit"></i> Sunting</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/admin/delete/'.$i->id_admin) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>

						<div class="row">
							<div class="col">
							<?php echo $pagination;?>
							</div>
						</div>

						<?php endif; ?>

					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php //$this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
		$('#btn-delete').attr('href',url);
		$('#deleteModal').modal();
		}

		$(document).ready(function(){

			$('#table_admin').DataTable({
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 2 ],
					orderData: [ 2, 0 ]
				} ]
			});

			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});

	</script>

</body>

</html>