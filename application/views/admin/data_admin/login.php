
<!DOCTYPE <!DOCTYPE html>
<html>

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
    <style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    h1{
       margin: 50px 300px 0px;        
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>

<body>

<?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Berhasil login,</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } else if($this->session->flashdata('error')){  ?>

        <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Tidak berhasil login, </strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>



<h1 class="text-center">Website Computational Thinking Teknik Informatika UNPAR</h1>
<break>
<div class="login-form">

    <form action="<?php echo base_url('index.php/admin/login') ?>" method="post">
        <h2 class="text-center">Log in</h2>       
        <div class="form-group">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
    </form>

</div>
</body>

</html>