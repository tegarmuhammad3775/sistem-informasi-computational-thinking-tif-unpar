<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!--breadcrumbs disini-->

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_negara')): ?>
				<div class="alert alert-danger" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_negara'); ?>
				</div>
				<?php endif; ?>
				
				<br>
				<h5 class="font-weight-bold text-center">Tambah Negara</h5>
				<br>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/negara/')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/negara/add') ?>" method="post">

							<div class="form-group">
								<label for="nama_negara">Negara*</label>
								<input class="form-control <?php echo form_error('nama_negara') ? 'is-invalid':'' ?>"
								 type="text" name="nama_negara" placeholder="Negara" />
								<div class="invalid-feedback">
									<?php echo form_error('nama_negara') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="negara_singkatan">Singkatan Negara*</label>
								<input class="form-control <?php echo form_error('negara_singkatan') ? 'is-invalid':'' ?>"
								 type="text" name="negara_singkatan" placeholder="Singkatan Negara" />
								<div class="invalid-feedback">
									<?php echo form_error('negara_singkatan') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->


			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>
	$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
</script>

</html>