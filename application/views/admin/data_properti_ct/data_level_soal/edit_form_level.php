<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

			<!--session-->

			    <?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_level')): ?>
				<div class="alert alert-danger" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_level'); ?>
				</div>
				<?php endif; ?>
				

				<br>	
				<h5 class="font-weight-bold text-center">Sungting Data Level Soal CT</h5>
				<br>


				
				<!-- Card  -->
				<div class="card mb-3">	
					<div class="card-header">

						<a href="<?php echo site_url('admin/level_soal_ct/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/level_soal_ct/edit') ?>" method="post">

							<input type="hidden" name="id_level_soal" value="<?php echo $level_ct_id->id_level_soal?>" />

							<div class="form-group">
								<label for="level_soal">Level Soal CT*</label>
								<input class="form-control
								 <?php echo form_error('level_soal') ? 'is-invalid':'' ?>"
								 type="text" name="level_soal" value="<?php echo htmlspecialchars($level_ct_id->level_soal)?>" />
								<div class="invalid-feedback">
									<?php echo form_error('level_soal') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="keterangan">Keterangan*</label>
								<input class="form-control <?php echo form_error('keterangan') ? 'is-invalid':'' ?>"
								 type="text" name="keterangan" value="<?php echo htmlspecialchars($level_ct_id->keterangan)?>" />
								<div class="invalid-feedback">
									<?php echo form_error('keterangan') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>	
		$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
</script>

</html>