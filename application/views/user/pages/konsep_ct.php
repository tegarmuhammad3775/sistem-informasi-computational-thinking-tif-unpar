<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    <a class="navbar-brand " href="<?php echo site_url('user/home')?>">Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>


<div class="container">
<h3 class="title text-center" style="font-weight: bold">Computational Thinking</h3>
<p class="text-justify">
<i>Computational Thinking</i> (CT) adalah proses berpikir untuk memformulasikan persoalan dan solusinya, 
sehingga solusi tersebut secara efektif dilaksanakan oleh sebuah agen pemroses informasi ("komputer", robot, atau manusia). 
CT adalah sebuah cara berpikir untuk memecahkan persoalan, merancang sistem, memahami perilaku manusia. 
CT melandasi konsep informatika. Di dunia saat ini dimana komputer ada di mana-mana untuk membantu berbagai segi kehidupan,
CT harus menjadi dasar bagaimana seseorang berpikir dan memahami dunia dengan persoalan-persoalannya yang semakin kompleks. 
CT berarti berpikir untuk menciptakan dan menggunakan beberapa tingkatan abstraksi, mulai memahami persoalan sehingga
mengusulkan pemecahan solusi yang efektif, efisien, “fair” dan aman. CT berarti memahami konsekuensi dari skala persoalan 
dan kompleksitasnya, tak hanya demi efisiensi, tetapi juga untuk alasan ekonomis dan sosial.
CT merupakan teknik yang membantu kita untuk memecahkan masalah
(problem solving) dengan memahami permasalahan yang ada kemudian merumuskan solusi - solusi
yang mungkin untuk kemudian menyajikan solusi tersebut dalam bentuk yang dapat dipahami
manusia maupun komputer. CT terdiri dari 6 konsep yang berbeda yaitu
<i>Logical Reasoning</i>, <i>Algorithms</i>, <i>Decomposition</i>, <i>Patterns</i>, <i>Abstraction</i>, dan <i>Evaluaton</i>.
</p>


  <div class="accordion-option">
    <h2 class="title">Konsep Computational Thinking</h2>
    <a href="javascript:void(0)" class="toggle-accordion active" accordion-id="#accordion"></a>
  </div>
  <div class="clearfix"></div>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Logical Reasoning
        </a>
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	  
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct1.jpg" class="rounded" alt="..." title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
		<p class="text-justify"><i>Logical reasoning</i> atau Penalaran Logis membantu kita menjelaskan mengapa sesuatu hal terjadi.
		Proses menggunakan pengetahuan yang ada dari suatu sistem untuk membuat prediksi yang akan
		digunakan di masa yang akan datang adalah salah satu bagian dari penalaran logis. Inti dari
		penalaran logis adalah mampu menjelaskan mengapa sesuatu hal bisa terjadi. Contoh penggunaan
		penalaran logis adalah ketika kita menggerakkan mouse komputer kita dapat melihat pointer pada
		layar komputer mengikuti gerakan mouse komputer dan selanjutnya kita dapat memahami apabila
		kita menggerakkan mouse komputer maka pointer akan ikut bergerak mengikuti gerakkan mouse
		komputer.</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          Algorithms
        </a>
      </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct2.jpg" class="rounded" alt="..." height="50%" width="50%" title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
		  <br>
      <p class="text-justify"> <i>Algorithms</i> atau Algoritma adalah urutan instruksi atau seperangkat instruksi langkah demi
		langkah untuk melakukan suatu hal atau untuk memecahkan suatu masalah. Jika kita ingin
		memberitahu komputer untuk melakukan sesuatu, kita harus menjelaskan dengan tepat apa yang
		kita inginkan dan bagaimana kita ingin melakukannya dengan menulis langkah demi langkah dalam
		bentuk program komputer. Penulisan langkah demi langkah tersebut memerlukan perencanaan,
		dan untuk melakukan perencanaan tersebut maka digunakanlah algoritma. Contoh penggunaan
		algoritma sering kita jumpai dalam kehidupan sehari-hari seperti tahapan-tahapan untuk membuat
		roti panggang. Pertama kita perlu mengiris roti, kemudian panggang roti, selanjutnya oleskan mentega
		dan tambahkan selai, terakhir simpan pada piring untuk disajikan. </p>
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          Decomposition
        </a>
      </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct3.jpg" class="rounded" alt="..." title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
      <p class="text-justify"> <i>Decomposition</i> atau Dekomposisi adalah proses pemecahan masalah menjadi bagian yang lebih	
		mudah untuk dikelola dan dimengerti. Dekomposisi membantu kita memecahkan masalah yang
		rumit dan mengelola proyek-proyek yang besar. Dekomposisi membuat proses lebih mudah dikelola
		dan diselesaikan. Contoh penggunaan dekomposisi adalah ketika terdapat dua orang yang ingin
		membuat sarapan yang terdiri dari roti panggang dan teh pada waktu yang bersamaan. Agar lebih
		efektif, kedua orang tersebut perlu berbagi tugas, satu orang membuat roting panggang dan satu
		lainnya membuat teh, dengan melakukan hal tersebut tugas menjadi lebih mudah untuk dilakukan.	
    </p>
        </div>
      </div>
     </div>

	 <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingfour">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapseFour">
          Patterns
        </a>
      </h4>
      </div>
      <div id="collapsefour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct4.jpg" class="rounded" alt="..." title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
      <p class="text-justify"> <i>Patterns</i> atau Pola digunakan untuk mengidentifikasi persamaan dan perbedaan umum. Dengan
		mengidentifikasi pola, kita dapat membuat prediksi, membuat aturan, dan memecahkan masalah
		yang lebih umum. Contoh penggunaan patterns adalah ketika kita ingin mehitung luas suatu persegi
		panjang, kita dapat menghitung secara manual jumlah kotak-kotak yang terdapat di dalam persegi
		tersebut. Namun, solusi yang lebih baik adalah dengan mengalikan panjang dari persegi dengan
		lebar dari persegi. Metode pengalian panjang dan lebar tersebut bukan hanya cepat tetapi dapat
		digunakan untuk semua persegi panjang dengan panjang dan lebar yang berbeda.</p>
        </div>
      </div>
    </div>

	<div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingfive">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-controls="collapseFive">
          Abstraction
        </a>
      </h4>
      </div>
      <div id="collapsefive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive">
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct5.jpg" class="rounded" alt="..." title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
		  <br>
      <p class="text-justify"> <i>Abstraction</i> atau Abstraksi adalah tentang menyederhankan suatu hal. Abstraksi berarti hanya
		fokus pada hal yang penting dan mengabaikan hal yang kurang penting. Abstraksi memungkinkan
		kita untuk mengelola kerumitan. Jadwal sekolah adalah salah satu contoh dari abstraksi. Jadwal
		sekolah berisi informasi penting yang diperlukan siswa untuk menghadiri suatu kelas, seperti pada
		hari apa saja siswa perlu datang ke sekolah, pada jam tertentu seorang siswa akan mempelajari
		suatu mata pelajaran tertentu, mata pelajaran apa saja yang akan dipelajari siswa di hari tertenju.
		Jadwal tidak berisi informasi mengenai tujuan dan detail mengenai pembelajaran karena hal tersebut
		8 Bab 2. Landasan Teori bukanlah bagian yang harus siswa ketahui untuk mengahadiri suatu kelas.</p>
        </div>
      </div>
    </div>

	<div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingsix">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="true" aria-controls="collapseSix">
          Evaluation
        </a>
      </h4>
      </div>
      <div id="collapsesix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSix">
        <div class="panel-body">
		<div class="text-center">
 	 		<img src="<?php echo base_url(); ?>/assets/images/ct6.jpg" class="rounded" alt="..." title="https://www.barefootcomputing.org/concept-approaches/computational-thinking-concepts-and-approaches">
	 	 </div>
		  <br>
      <p class="text-justify"> <i>Evaluation</i> atau Evaluasi adalah proses yang memungkinkan kita untuk memastikan solusi yang
		kita buat bekerja dengan baik serta memikirkan bagaimana solusi tersebut bisa diperbaiki. Evaluasi
		memungkinkan kita untuk mempertimbangkan solusi yang kita buat untuk menyelesaikan suatu
		masalah, memastikan bahwa solusi telah memenuhi kriteria yang diharapkan, sehingga menghasilkan
		solusi yang tepat dan sesuai dengan tujuan yang diharapkan sebelum proses pemrograman dimulai.
		Contoh evaluasi adalah ketika mempertimbangkan perangkat digital baru untuk digunakan di kelas,
		akan terdapat sejumlah kriteria yang akan dipertimbangkan, misalnya sistem operasi, portabilitas,
		memori, ukuran layar, kemudahan pengguaan dan garansi.</p>
        </div>
      </div>
    </div>
	 
 </div>
  </div>
  
</div>

<?php $this->load->view("user/_partials/footer.php") ?>

<script>
$(document).ready(function() {

$(".toggle-accordion").on("click", function() {
  var accordionId = $(this).attr("accordion-id"),
    numPanelOpen = $(accordionId + ' .collapse.in').length;
  
  $(this).toggleClass("active");

  if (numPanelOpen == 0) {
    openAllPanels(accordionId);
  } else {
    closeAllPanels(accordionId);
  }
})

openAllPanels = function(aId) {
  console.log("setAllPanelOpen");
  $(aId + ' .panel-collapse:not(".in")').collapse('show');
  $(function () {
     $('a').each(function() {
         $(this).attr('aria-expanded', 'true')
     });
});
}
closeAllPanels = function(aId) {
  console.log("setAllPanelclose");
  $(aId + ' .panel-collapse.in').collapse('hide');
  $(function () {
     $('a').each(function() {
         $(this).attr('aria-expanded', 'false')
     });
});
}
   
});

$(function () { 
    $('a').on('click', function (e) {
        var menuItem = $( e.currentTarget );

        if (menuItem.attr( 'aria-expanded') === 'true') {
            $(this).attr( 'aria-expanded', 'false');
        } else {
            $(this).attr( 'aria-expanded', 'true');
        }
    });
});
</script>

<script>
$( '#topheader .navbar-nav a' ).on( 'click', function () {
	$( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
	$( this ).parent( 'li' ).addClass( 'active' );
});
</script>