<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<?php $this->load->view("user/_partials/nav.php") ?>

<div class="container--head ">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <div class="carousel-tooltip">
      <div class="caraousel-tooltip-item active" data-index="0">
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
      </div>
      <div class="caraousel-tooltip-item" data-index="1">
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
      </div>
      <div class="caraousel-tooltip-item" data-index="2">
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
      </div>
      <div class="caraousel-tooltip-item" data-index="3">
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
      </div>
      <div class="caraousel-tooltip-item" data-index="4">
        <a href="#" class="tooltip-carousel" style="" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        </a>
      </div>
    </div>

    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      
      <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/k12_1.jpg" style="width:100%;height:500px">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor</h3> Lorem ipsum dolor sit amet, tharsiam eam est in.
        </div>
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>/assets/images/k12_2.jpg" class="img-fluid" alt="..." 
             
             style="width:100%;height:500px">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor</h3> Lorem ipsum dolor sit amet, tharsiam eam est in.
        </div>
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>/assets/images/k12_3.jpg" class="img-fluid" alt="..." style="width:100%;height:500px">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor</h3> Lorem ipsum dolor sit amet, tharsiam eam est in.
        </div>
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>/assets/images/k12_4.jpg" class="img-fluid" alt="..." style="width:100%;height:500px">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor</h3> Lorem ipsum dolor sit amet, tharsiam eam est in.
        </div>
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>/assets/images/k12_5.jpg" class="img-fluid" alt="..." style="width:100%;height:500px">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor</h3> Lorem ipsum dolor sit amet, tharsiam eam est in.
        </div>
      </div>
      </div>



    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div>
</div>




<!-- Page Content -->
<div class="container">

  <!-- Page Heading -->
   <h1 class="my-4 text-center">Computational Thinking </h1>
   <p class="text-justify"> <i>Computational Thinking (CT)</i> adalah proses berpikir untuk memformulasikan persoalan dan solusinya, sehingga
      solusi tersebut secara efektif dilaksanakan oleh sebuah agen pemroses informasi ("komputer", robot, atau manusia).
      CT adalah sebuah cara berpikir untuk memecahkan persoalan, merancang sistem, memahami perilaku manusia. CT melandasi 
      konsep informatika. Di dunia saat ini dimana komputer ada di mana-mana untuk membantu berbagai segi kehidupan, CT harus 
      menjadi dasar bagaimana seseorang berpikir dan memahami dunia dengan persoalan-persoalannya yang semakin kompleks. CT berarti
       berpikir untuk menciptakan dan menggunakan beberapa tingkatan abstraksi, mulai memahami persoalan sehingga mengusulkan
        pemecahan solusi yang efektif, efisien, “fair” dan aman. CT berarti memahami konsekuensi dari skala persoalan dan
         kompleksitasnya, tak hanya demi efisiensi, tetapi juga untuk alasan ekonomis dan sosial. CT dari 6 konsep yang berbeda yaitu
        <i>Logical Reasoning</i>, <i>Algorithms</i>, <i>Decomposition</i>, <i>Patterns</i>, <i>Abstraction</i>, dan <i>Evaluation</i>.
      </p>

  <div class="row">
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/penalaran_logis.jpg" alt=""
        title="https://www.barefootcomputing.org/concepts-and-approaches/logic"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>"> Logical Reasoning</a>
          </h4>
          <p class="card-text text-justify"> <i>Logical reasoning</i> atau Penalaran Logis membantu kita menjelaskan mengapa sesuatu hal terjadi.
            Proses menggunakan pengetahuan yang ada dari suatu sistem untuk membuat prediksi yang akan
            digunakan di masa yang akan datang adalah salah satu bagian dari penalaran logis.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/algoritma.jpg" alt=""
        title="https://www.barefootcomputing.org/concepts-and-approaches/algorithms"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>">Algorithms</a>
          </h4>
          <p class="card-text text-justify"> <i>Algorithms</i> atau Algoritma adalah urutan instruksi atau seperangkat instruksi langkah demi
          langkah untuk melakukan suatu hal atau untuk memecahkan suatu masalah.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/dekomposisi.jpg" alt=""
        title="https://www.barefootcomputing.org/concepts-and-approaches/decomposition"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>">Decomposition</a>
          </h4>
          <p class="card-text text-justify"> <i>Decomposition</i> atau Dekomposisi adalah proses pemecahan masalah menjadi bagian yang lebih
            mudah untuk dikelola dan dimengerti. Dekomposisi membantu kita memecahkan masalah yang
            rumit dan mengelola proyek-proyek yang besar.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/pengenalan_pola.jpg" alt=""
           title="https://www.barefootcomputing.org/concepts-and-approaches/patterns"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>">Patterns</a>
          </h4>
          <p class="card-text text-justify"> <i>Patterns</i> atau Pola digunakan untuk mengidentifikasi persamaan dan perbedaan umum. Dengan
          mengidentifikasi pola, kita dapat membuat prediksi, membuat aturan, dan memecahkan masalah
          yang lebih umum.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/abstraksi.jpg" alt=""
           title="https://www.barefootcomputing.org/concepts-and-approaches/abstraction"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>">Abstraction</a>
          </h4>
          <p class="card-text text-justify"> <i>Abstraction</i> atau Abstraksi adalah tentang menyederhankan suatu hal. Abstraksi berarti hanya
            fokus pada hal yang penting dan mengabaikan hal yang kurang penting. Abstraksi memungkinkan
            kita untuk mengelola kerumitan.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>/assets/images/evaluasi.jpg" alt=""
        title="https://www.barefootcomputing.org/concepts-and-approaches/evaluation"
        ></a>
        <div class="card-body">
          <h4 class="card-title">
          <a href="<?php echo site_url('user/konsep_ct')?>">Evaluation</a>
          </h4>
          <p class="card-text text-justify"> <i>Evaluation</i> atau Evaluasi adalah proses yang memungkinkan kita untuk memastikan solusi yang
kita buat bekerja dengan baik serta memikirkan bagaimana solusi tersebut bisa diperbaiki.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <!-- Pagination
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">1</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">2</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">3</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
    </li>
  </ul>
   -->


</div>
<?php $this->load->view("user/_partials/footer.php") ?> 

