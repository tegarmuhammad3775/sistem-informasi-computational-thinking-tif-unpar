<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>

</head>

<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    <a class="navbar-brand " href="<?php echo site_url('user/home')?>">Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>

<div class = "card-header"> 
       <h4 class = "text-center">Liputan Seputar Computational Thinking oleh Teknik Informatika UNPAR</h4>
</div>

<?php
//date('l , d F Y',$this->tanggal)
?>


      <div class="card" style="padding:20px">
        <?php if (!empty($data_liputan)): ?>
        <?php foreach ($data_liputan as $i): ?>
          <div class="card-body">
            <!--<p class="card-title" style="font-size:18px"><strong><?php// echo $i->judul_liputan?></strong></p>-->
            <p class="card-title" style="font-size:18px">
                 <strong>
                    <a href="<?php echo base_url();?>index.php/user/liputan_ct/page_liputan_ct/<?php echo $i->id_liputan?>"><?php echo $i->judul_liputan?></a>
                </strong>
            </p>
            <p>
              <?php 
                  $tgl = $i->tanggal;
                  $res = $this->controller->tanggal_indo(date('Y-m-d', strtotime($tgl)));
                  echo $res;
              ?>
            </p>

        <div class="row">
          <div class="col-sm-3">
                <?php
                      $id_liputan = $i->id_liputan;
                      $id_image = $this->controller->get_first_imageLiputan($id_liputan);
  
                      if($id_image != false){
                        echo '
                                  <img class="img-fluid" style="vertical-align:middle;width:30rem;height:20rem;" src="data:image/jpeg;base64,'.$this->controller->display_gambar_liputan($id_image).'"/>
                              ';
                      }
                      else{
                        echo '<div style="width:30rem;height:20rem; display:flex; justify-content:center; align-items:center;">
                        <img class="img-fluid" style="width:auto;height:18rem" src="';
                        echo base_url();
                        echo '/assets/images/logo.jpg" alt="Card image cap">';
                        echo '</div>';
                      }
                ?>
                
            </div>
              
            <div class="col-sm-7">
              
              <?php 
                    $temp_liputan = $i->deskripsi_liputan;
                    $num_words = 51;
                    $words = array();
                    $words = explode(" ", $temp_liputan, $num_words);
                    $shown_string = "";
                    
                    if(count($words) == 51){
                      $words[50] = " ... ";
                    }

                    $shown_string = implode(" ", $words);
                    echo $shown_string;
              
              ?>
              
               <br>
               <a style="margin-top:10px" href="<?php echo base_url();?>index.php/user/liputan_ct/page_liputan_ct/<?php echo $i->id_liputan?>" class="btn btn-primary">Selengkapnya <i class="fas fa-arrow-right"></i></a>              

            </div>

          </div>

         

      </div>
 

          <?php  endforeach; ?>
      
      <div class="row">
        <div class="col">
        <?php echo $pagination;?>
        </div>
      </div>
          
      <?php endif; ?>
          



      </div>






<?php $this->load->view("user/_partials/footer.php") ?> 


<script>
$( '#topheader .navbar-nav a' ).on( 'click', function () {
	$( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
	$( this ).parent( 'li' ).addClass( 'active' );
});
</script>