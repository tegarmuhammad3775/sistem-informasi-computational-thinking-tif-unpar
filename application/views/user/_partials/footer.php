  <!-- Site footer -->

  <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify"> Computational Thinking (CT) adalah proses berpikir untuk memformulasikan persoalan dan solusinya, sehingga
      solusi tersebut secara efektif dilaksanakan oleh sebuah agen pemroses informasi ("komputer", robot, atau manusia).
      CT adalah sebuah cara berpikir untuk memecahkan persoalan, merancang sistem, memahami perilaku manusia. CT melandasi 
      konsep informatika.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Konsep CT</h6>
            <ul class="footer-links">
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Logical Reasoning</a></li>
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Algorithms</a></li>
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Decomposition</a></li>
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Patterns</a></li>
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Abstraction</a></li>
              <li><a href="<?php echo site_url('user/konsep_ct')?>">Evaluation</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://informatika.unpar.ac.id/">About Us</a></li>
              <li><a href="<?php echo site_url('user/contact')?>">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text"> &copy 2019, Program Studi Informatika, Universitas Katolik Parahyangan.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              
              <li><a class="facebook" href="https://www.facebook.com/if.unpar/"><i class="fab fa-facebook-f"></i></a></li>
              <li><a class="twitter" href="https://twitter.com/if_unpar"><i class="fab fa-twitter"></i></a></li>
              <li><a class="youtube" href="https://www.youtube.com/user/unparofficial"><i class="fab fa-youtube"></i></a></li>  
              <li><a class="instagram" href="https://www.instagram.com/if.unpar/"><i class="fab fa-instagram"></i></a></li> 
            </ul>
          </div>
        </div>
      </div>
</footer>