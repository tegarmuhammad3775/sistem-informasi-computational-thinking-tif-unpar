<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->helper('form'); 
	}

	public function index()
	{
		$this->load->view('user/pages/contact');
	}

	function send(){
		//$this->load->config('email');
		$this->load->library('email');

		$from = 'cttifunpar@gmail.com';
		$to = 'tegarmuhammad378@gmail.com';
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');


		// Mail content
		$mailContent = '
						<html>
							<h2>Contact Request Submitted</h2>
							<pre><b>Name	: '.$nama.'</b></pre>
							<pre><b>Email  	: '.$email.'</b></pre>
							<pre><b>Subject : '.$subject.'</b></pre>
							<pre><b>Message : '.$message.'</b></pre>
						</html>';
		
	    $this->email->set_header('Content-Type', 'text/html');
		$this->email->set_newline("\r\n");
		$this->email->from($from,'Noreply CTTIFUNPAR');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($mailContent);

		if ($this->email->send()) {
		   // echo 'Your message has successfully been sent.';
		   $this->session->set_flashdata('success', 'Your message has successfully been sent.');
		   redirect('user/contact');
        } else{
            show_error($this->email->print_debugger());
		}
		
		
    }
}