<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liputan_CT extends CI_Controller {
	public $controller = null;

	public function __construct(){
		parent::__construct();
		$this->load->model("UploadGambar_model");
		$this->load->model("LiputanCT_model");
		$this->load->library('pagination');
		$this->controller = & get_instance();
	}

	public function index(){
		$this->load->database();
		$jumlah_data_liputan = $this->LiputanCT_model->jumlah_data_liputan();
	
		$config['base_url'] = base_url("index.php/user/liputan_ct/index/");
		$config['total_rows'] = $jumlah_data_liputan;
		$config['per_page'] = 4;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);

		// Membuat Style pagination untuk BootStrap v4
      	$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		
		$this->pagination->initialize($config);	

		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['data_liputan'] = $this->LiputanCT_model->get_data_liputan($config['per_page'],$data['page']);
		$data['pagination'] = $this->pagination->create_links();
		$data["liputan_ct"] = $this->LiputanCT_model->getAll_desc();
		
		$this->load->view('user/pages/liputan_ct_view',$data);
		
	}

	public function page_liputan_ct(){
		$id=$this->uri->segment(4);
		$data["data_liputan_ct"] = $this->LiputanCT_model->get_liputan_by_id($id);
		$data['data_imageLiputan'] = $this->UploadGambar_model->get_id_imageLiputan($id);
		$this->load->view('user/pages/liputan_ct_page',$data);
	}

	function display_gambar_liputan($id_gambar=null){
        return $this->UploadGambar_model->display_gambar_liputan($id_gambar);
	}

	function get_first_imageLiputan($id_liputan){
		return $this->UploadGambar_model->get_first_imageLiputan($id_liputan);
	}

	function tanggal_indo($tanggal){
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$temp = explode('-', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
	 
		return $temp[2] . ' ' . $bulan[ (int)$temp[1] ] . ' ' . $temp[0];
	}
}