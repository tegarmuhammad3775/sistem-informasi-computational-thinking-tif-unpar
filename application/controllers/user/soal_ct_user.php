<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal_CT_User extends CI_Controller {

	public $controller = null;

	public function __construct(){
		parent::__construct();
		$this->load->model("SoalCT_model");
		$this->load->model("PembahasanSoalCT_model");
		$this->load->model("UploadGambar_model");
		$this->load->model("TagCT_soal_model");
		$this->load->model("KonsepCT_Soal_model");
		$this->load->model('KonsepCT_model');
		$this->load->model("PencarianSoalCT_model");
		$this->load->model("KategoriUmurCT_model");
		$this->load->model("Negara_model");
		$this->controller = & get_instance();
	}

	public function index(){ 
		
		$data["soal_ct"] = $this->SoalCT_model->getAll();
		$data["data_konsep_ct"] = $this->SoalCT_model->get_konsep_ct();
		$data["data_kategori_umur"] = $this->SoalCT_model->get_kategori_umur();
		$data["data_tag_soal"] = $this->SoalCT_model->get_tag_soal();
		$data['konsep_ct'] = $this->KonsepCT_model->getAll();
		$data['id_konsep_ct']=$this->KonsepCT_Soal_model->get_idKonsepCT_by_idSoalCT(0);
		$data["jenjang_pendidikan"] = $this->KategoriUmurCT_model->get_jenjang_pendidikan();
		$data["selected_konsep"]=$this->PencarianSoalCT_model->get_selected_konsep();
		$data["selected_tipeSoal"]=$this->PencarianSoalCT_model->get_selected_tipeSoal();
		$data["selected_jenjangPendidikan"] = $this->PencarianSoalCT_model->get_selected_jenjangPendidikan();
		$data["selected_umurAwal"] = $this->PencarianSoalCT_model->get_selected_umurAwal();
		$data["selected_umurAkhir"] = $this->PencarianSoalCT_model->get_selected_umurAkhir();
		$data["soal_relevan"] = $this->PencarianSoalCT_model->search_soal_ct();
		

		$this->load->view('user/pages/soal_view',$data);
	}


	public function page_soal_ct(){
		$id=$this->uri->segment(4);
		$data["soal_ct"] = $this->SoalCT_model->getAll();
		$data['data_soal'] = $this->SoalCT_model->per_id($id);
		$data['data_pembahasan'] = $this->PembahasanSoalCT_model->per_id($id);
		$data['data_imageSoal'] = $this->UploadGambar_model->get_id_imageSoalDeskripsi($id);
		$data['data_imagePertanyaan']  = $this->UploadGambar_model->get_id_imageSoalPertanyaan($id);
		$data['data_imageJawaban'] = $this->UploadGambar_model->get_id_imagePembahasanJawaban($id);
		$data['data_imagePenjelasan'] = $this->UploadGambar_model->get_id_imagePembahasanPenjelasan($id);
		$data['data_tag'] = $this->TagCT_soal_model->getTag_by_idSoalCT($id);
		$data['data_konsepCT'] = $this->KonsepCT_Soal_model->getKonsepCT_by_idSoalCT($id);
		$data['data_kategori_umur'] = $this->SoalCT_model->get_kategori_umur();
		$data['data_negara'] = $this->SoalCT_model->get_negara_soal($id);
		
		$this->load->view('user/pages/soal_ct_page',$data);
	}

	function display_gambar($id_gambar=null){
        return $this->UploadGambar_model->display_gambar($id_gambar);
	}

	function get_first_imageSoal($id_soal_ct){
		return $this->UploadGambar_model->get_first_imageSoal($id_soal_ct);
	}

	function get_first_imagePembahasan($id_soal_ct){
		return $this->UploadGambar_model->get_first_imagePembahasan($id_soal_ct);
	}

	public function laporan_pdf(){
	
		$this->load->library('pdf');

		$id=$this->uri->segment(4);
		$data["soal_ct"] = $this->SoalCT_model->getAll();
		$data['data_soal'] = $this->SoalCT_model->per_id($id);
		$data['data_pembahasan'] = $this->PembahasanSoalCT_model->per_id($id);
		$data['data_imageSoal'] = $this->UploadGambar_model->get_id_imageSoalDeskripsi($id);
		$data['data_imagePertanyaan']  = $this->UploadGambar_model->get_id_imageSoalPertanyaan($id);
		$data['data_imageJawaban'] = $this->UploadGambar_model->get_id_imagePembahasanJawaban($id);
		$data['data_imagePenjelasan'] = $this->UploadGambar_model->get_id_imagePembahasanPenjelasan($id);
		$data['data_tag'] = $this->TagCT_soal_model->getTag_by_idSoalCT($id);
		$data['data_konsepCT'] = $this->KonsepCT_Soal_model->getKonsepCT_by_idSoalCT($id);
		$data['data_kategori_umur'] = $this->SoalCT_model->get_kategori_umur();
		$data['data_negara'] = $this->SoalCT_model->get_negara_soal($id);

		$judul_soal = $this->SoalCT_model->get_judul_soal($id);

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "Soal CT - ".$judul_soal;

		

		$this->pdf->load_view('user/pages/laporan_pdf', $data);
	}
	
	
}