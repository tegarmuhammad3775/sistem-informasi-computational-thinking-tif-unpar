<?php
defined('BASEPATH') OR exit('No direct script allowed');

Class Liputan_CT extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('LiputanCT_model');   //meload objek model PembahasanSoalCT_model
        $this->load->model("UploadGambar_model");
        $this->load->library('form_validation');        //meload library form validation
        $this->load->library('pagination');             // meload library pagination
        $this->controller = & get_instance();
    }

    public function index(){
        //method ini mengambil data dari pembahasansoalct_model dengan memanggil getAll() 
        //lalau menampilkannya ke view

        $data["liputan_ct"] = $this->LiputanCT_model->getAll();

        $config['base_url'] = base_url("index.php/admin/liputan_ct/index/");
        $config['total_rows'] = $this->LiputanCT_model->jumlah_data_liputan();
        $config['per_page'] = $this->LiputanCT_model->jumlah_data_liputan();
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        

        $this->pagination->initialize($config);	
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["data_liputan"] = $this->LiputanCT_model->get_data_liputan($config['per_page'],$data['page']);
		$data['pagination'] = $this->pagination->create_links();


        $this->load->view('admin/data_liputan/list_liputan_ct',$data);
        
    }

    public function add(){
        //method ini menampilkan form add soal dan menyampan pembahasan soal ke database
        //memanggil method save pada PembahasanSoalCT_model
        //method run() melakukan validasi

//        $this->session->unset_userdata('success');

        
        $validation = $this->LiputanCT_model->validate_form();

        if($validation == TRUE){                                              //menjalankan validasi
           $this->LiputanCT_model->save();
           $this->session->set_flashdata('success','Berhasil disimpan');    //menampilkan pesan berhasil
        }

        $this->load->view('admin/data_liputan/new_form_liputan'); //menampilkan form add pembahasan
       
    }

    public function edit($id){

        
      //  print_r($this->input->post());
        
        //$id = id data yang akan diedit, nilai awal null
       
        
        if( !isset($id)){                                                            //melakuan redirect jika tidak ada id
             redirect('admin/liputan_ct');                                      
        }
        
        $liputan_ct = $this->LiputanCT_model;

        $validation = $liputan_ct->validate_form();                                          //objek form_validation

        if($validation == TRUE){                                                          //menjalankan validasi
            $liputan_ct->update();                                              //menyimpan data ke database
        }

        $data["liputan_ct"] = $this->LiputanCT_model->get_liputan_by_id($id);                          //mengambil data untuk ditampilkan pada form
        $data['data_imageLiputan'] = $this->UploadGambar_model->get_id_imageLiputan($id);

        if( !$data["liputan_ct"]){                                             //jika tida ada, tampilkan error 404
           show_404();                                                                 
        } 

        $this->load->view('admin/data_liputan/edit_form_liputan',$data);    //menampilkan form edit pembahasan soal

    }

    public function delete($id){
                                                                                        //menghapus data soal
                                                                                        //id data yang akan delete, nilai awal null
                                                                                        //apabila data berhasil dihapus di alihkan/redirect ke halaman admin

        if(!isset($id)){                                                                //melakuan redirect jika tidak ada id
            show_404(); 
        }
        
        $this->UploadGambar_model->delete_imageLiputan($id);
        $this->LiputanCT_model->delete($id);
        redirect('admin/liputan_ct/');
    }

    public function tampilkanDataLiputan(){
        $id=$this->uri->segment(4);
        $data['data_liputan'] = $this->LiputanCT_model->get_liputan_by_id($id);
        $data['data_imageLiputan'] = $this->UploadGambar_model->get_id_imageLiputan($id);
        
		$this->load->view('admin/data_liputan/halaman_liputan',$data);
    }

    function display_gambar_liputan($id_gambar=null){
        return $this->UploadGambar_model->display_gambar_liputan($id_gambar);
    }


    function delete_image_liputan($id_image){ 
        
        $id_liputan = $this->UploadGambar_model->delete_image_liputan($id_image);
        $this->LiputanCT_model->update();
        redirect('admin/liputan_ct/edit/'.$id_liputan);
    }

    function tanggal_indo($tanggal){
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$temp = explode('-', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
	 
		return $temp[2] . ' ' . $bulan[ (int)$temp[1] ] . ' ' . $temp[0];
	}


}