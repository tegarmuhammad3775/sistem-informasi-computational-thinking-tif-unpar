<?php
defined('BASEPATH') OR exit('No direct script allowed');

class Admin extends CI_Controller{

    public function __construct(){
        parent::__construct();
     
        $this->load->model("Admin_model"); //meload admin_model
        $this->load->library('form_validation'); //meload library form_validation
        $this->load->library('pagination'); // meload library pagination
    }

    public function index(){
        //method ini mengambil data dari admin_model dengan memanggil getAll() 
        //lalau menampilkannya ke view
        

        $data["admin"] = $this->Admin_model->getAll();

        $config['base_url'] = base_url("index.php/admin/admin/index/");
        $config['total_rows'] = $this->Admin_model->jumlah_data_admin();
        $config['per_page'] = $this->Admin_model->jumlah_data_admin();
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        

        $this->pagination->initialize($config);	
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["data_admin"] = $this->Admin_model->get_data_admin($config['per_page'],$data['page']);
		$data['pagination'] = $this->pagination->create_links();
        
        $this->load->view("admin/data_admin/list", $data);

    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('admin/login');

    }

    public function add(){
        //method ini menampilkan form add dan menyampan data ke database
        //memanggil method save pada admin_model
        //method run() melakukan validasi

        $admin = $this->Admin_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation -> set_rules($admin->rules()); // menerapkan rules

        if($validation->run()){ //melakukan validasi
             
            if($admin->save() == true){
                $this->session->set_flashdata('success','Berhasil disimpan'); //menampilkan pesan berhasil
                redirect('admin/admin/add'); 
            }
            else{
                $this->session->set_flashdata('gagal_admin', 'Terjadi kesalahan : Nama, username, atau password telah digunakan!');
            }
        }

        $this->load->view("admin/data_admin/new_form"); //menampilkan form add

    }

    public function edit($id = null){
        //$id = id data yang akan diedit, nilai awal null
        
        if(!isset($id)){
            $this->session->sess_destroy();
            redirect('admin/admin'); //melakuan redirect jika tidak ada id
        }

        $admin = $this->Admin_model; //objek model
        $validation = $this->form_validation; // objek validation
        $validation->set_rules($admin->rules()); //menerapkan rules

        if($validation->run()){ //melakukan validasi
            
            if($admin->update() == true){
                $this->session->set_flashdata('success','Berhasil disimpan');
                redirect('admin/admin/edit/'.$id); 
            }
            else{
                $this->session->set_flashdata('gagal_admin', 'Terjadi kesalahan : Nama, username, atau password telah digunakan!');
            }
        }
        
        $data["admin"] = $admin->getById($id); //mengambil data untuk ditampilakn pada form
        if(!$data["admin"]){                   //jika tida ada, tampilkan error 404
            show_404();
        }

        $this->load->view("admin/data_admin/edit_form",$data); //menampilkan form edit
    }

    public function delete($id = null){
        //menghapus data
        //id data yang akan delete, nilai awal null
        //apabila data berhasil dihapus di alihkan/redirect ke halaman admin

        if(!isset($id)){
            show_404();
        }

        if($this->Admin_model->delete($id)){

            redirect(site_url('admin/admin'));
            
        }
    }

    

   

}