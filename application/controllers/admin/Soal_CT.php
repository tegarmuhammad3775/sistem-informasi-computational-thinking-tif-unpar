<?php
defined('BASEPATH') OR exit('No direct script allowed');

Class Soal_CT extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("SoalCT_model"); //meload admin_model
        $this->load->model("PembahasanSoalCT_model"); //meload admin_model
        $this->load->model("UploadGambar_model");
        $this->load->model("TagCT_Soal_model");
        $this->load->model("TagCT_model");
        $this->load->model("KonsepCT_Soal_model");
        $this->load->model('KonsepCT_model');
        $this->load->model('KategoriLevelSoalCT_model');
        $this->load->model('KategoriUmurCT_model');
        $this->load->model('LevelSoalCT_model');
        $this->load->model("Negara_model");
        $this->load->model('ImageSoalCT_model');
        $this->load->model('ImagePembahasanCT_model');
        $this->load->library("form_validation"); //meload library form validation
        $this->load->library('pagination');  // meload library pagination
        $this->controller = & get_instance();
    }

    public function index(){
        //method ini mengambil data dari soalct_model dengan memanggil getAll() 
        //lalau menampilkannya ke view
        
       $data["soal_ct"] = $this->SoalCT_model->getAll();
       $data["pembahasan_soal_ct"] = $this->PembahasanSoalCT_model->getAll();
       
       $config['base_url'] = base_url("index.php/admin/soal_ct/index/");
       $config['total_rows'] = $this->SoalCT_model->jumlah_data_soal();
       $config['per_page'] = $this->SoalCT_model->jumlah_data_soal();
       $config["uri_segment"] = 4;
       $choice = $config["total_rows"] / $config["per_page"];
       $config["num_links"] = floor($choice);

       // Membuat Style pagination untuk BootStrap v4
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['next_link']        = 'Next';
       $config['prev_link']        = 'Prev';
       $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
       $config['full_tag_close']   = '</ul></nav></div>';
       $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
       $config['num_tag_close']    = '</span></li>';
       $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
       $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
       $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
       $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
       $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
       $config['prev_tagl_close']  = '</span>Next</li>';
       $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
       $config['first_tagl_close'] = '</span></li>';
       $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
       $config['last_tagl_close']  = '</span></li>';
       

       $this->pagination->initialize($config);	
       $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
       $data["data_soal"] = $this->SoalCT_model->get_data_soal($config['per_page'],$data['page']);
       $data['pagination'] = $this->pagination->create_links();


       $this->load->view("admin/data_soal/list_soal",$data);
    
    }

    public function add(){
        $data['konsep_ct'] = $this->KonsepCT_model->getAll();
        $data['kategori_umur'] = $this->KategoriUmurCT_model->getAll();
        $data['level_soal'] = $this->LevelSoalCT_model->getAll();
        $data['tag_ct'] = $this->TagCT_model->getAll(); 
        $data['jumlah_tag'] = $this->TagCT_model->jumlah_data_tag();
        $data['negara_pembuat_soal'] = $this->Negara_model->getAll(); 
         
        if($this->input->post('finish')){
            $this->form_validation->set_rules('judul_soal','Judul Soal','trim|required');
            $this->form_validation->set_rules('tipe_soal','Tipe Soal','trim|required');
            $this->form_validation->set_rules('deskripsi_soal','Deskripsi Soal', 'trim|required');
            $this->form_validation->set_rules('pertanyaan','Pertanyaan', 'required');
            $this->form_validation->set_rules('negara_pembuat_soal','Negara Pembuat Soal', 'required');
            $this->form_validation->set_rules('jawaban','Jawaban','trim|required');
            $this->form_validation->set_rules('penjelasan','Penjelasan','required');

            $validation = $this->form_validation->run();
            
            $maxsize = 2097152;
            $max_img_soal = 0;
            $max_img_pembahasan = 0;
            
            $cek_image_soal = "true";
            $cek_image_pembahasan = "true";

            $arr_soal_size = [];
            $arr_soal_type = [];
            $arr_pembahasan_size = [];
            $arr_pembahasan_type = [];
            
            //validasi ukuran gambar soal dan tipe gambar soal
            if(!empty($_FILES['multipleFiles']['tmp_name'])){
               
                for($i = 0 ; $i < count($_FILES['multipleFiles']['size']); $i++){
                    
                    $arr_soal_size[$i] = $_FILES['multipleFiles']['size'][$i];
                    if($arr_soal_size[$i] > $max_img_soal){
                        $max_img_soal = $arr_soal_size[$i]; 
                    }

                    $arr_soal_type[$i] = $_FILES['multipleFiles']['type'][$i];
                    if($arr_soal_type[$i] == "image/jpeg" || $arr_soal_type[$i] == "image/gif" || $arr_soal_type[$i] == "image/png" || empty( $arr_soal_type[$i])){
                        $cek_image_soal = "true";
                    }
                    else{
                        $cek_image_soal = "false";
                        break;
                    }
                }
                /*
                echo ' cek_image_soal : '.$cek_image_soal." ";
                print_r($arr_soal_type);
                */

            }

            //validasi gambar pembahasan dan tipe gambar pembahasan
            if(!empty($_FILES['multiplePembahasan']['tmp_name'])){

                for($i = 0 ; $i < count($_FILES['multiplePembahasan']['size']); $i++){
                    $arr_pembahasan_size[$i] = $_FILES['multiplePembahasan']['size'][$i];
                    if($arr_pembahasan_size[$i] > $max_img_pembahasan){
                        $max_img_pembahasan = $arr_pembahasan_size[$i]; 
                    }

                    $arr_pembahasan_type[$i] = $_FILES['multiplePembahasan']['type'][$i];
                    if($arr_pembahasan_type[$i] == "image/jpeg" || $arr_pembahasan_type[$i] == "image/gif" || $arr_pembahasan_type[$i] == "image/png" || empty($arr_pembahasan_type[$i])){
                        $cek_image_pembahasan = "true";
                    }
                    else{
                        $cek_image_pembahasan = "false";
                        break;
                    }
                }
                /*
                echo ' cek_image_pembahasan : '.$cek_image_pembahasan." ";
                print_r($arr_pembahasan_type);
                echo $max_img_pembahasan;
                */
            }


            if($validation === TRUE){

                if($cek_image_soal == "true" && $cek_image_pembahasan == "true"){
                    if( $max_img_soal <= $maxsize && $max_img_pembahasan <= $maxsize){
                    
                        $dataSoalCT = $this->SoalCT_model->insert_soal_ct($this->input->post('judul_soal'),
                                                                         $this->input->post('tipe_soal'),
                                                                         $this->input->post('deskripsi_soal'),
                                                                         $this->input->post('pertanyaan'),
                                                                         $this->input->post('tahun'),
                                                                         $this->input->post('negara_asal_soal'),
                                                                         $this->input->post('negara_pembuat_soal'));

                        $id_soal_ct = $this->SoalCT_model->getLastID();

                        if($dataSoalCT === TRUE){
                            $dataPembahasanSoalCT = $this->PembahasanSoalCT_model->insert_pembahasan_soal_ct($id_soal_ct,$this->input->post('jawaban'),$this->input->post('penjelasan'),$this->input->post('penjelasan_ct'));
                        }
                        
                        //insert item konsep CT
                        if (!empty($this->input->post('konsepCT'))) {
                            foreach ($this->input->post('konsepCT') as $key => $val) {
                                $data0[] = array( 'konsepCT' => $_POST['konsepCT'][$key]);
                            }

                        }
            
                        foreach ($data0 as $item) {    
                        $idSoal = $this->SoalCT_model->getLastID();
                        $dataKonsepCT = $this->KonsepCT_Soal_model->insert_konsep_soal( $idSoal,$item['konsepCT']);
                        //print_r($item);
                        }

                        
                        //insert item tag CT
                        if(!empty($this->input->post('tagCT'))){
                            foreach($this->input->post('tagCT') as $key1 => $val1){
                                $data1[] = array('tagCT' => $_POST['tagCT'][$key1]);
                            }
                        }

                        foreach($data1 as $item){
                        $idSoal = $this->SoalCT_model->getLastID();
                        $dataTagCT = $this->TagCT_Soal_model->insert_tag_soal($idSoal,$item['tagCT']);
                        }


                        //insert item kategori umur dan level soal
                        if(!empty($this->input->post('kategori_umur'))){
                            foreach($this->input->post('kategori_umur') as $key2 => $val2){
                                $data2[] = array('kategori_umur' => $_POST['kategori_umur'][$key2],'level_soal' => $_POST['level_soal'][$key2]);
                            }   
                        }

                        foreach($data2 as $item){
                            $idSoal = $this->SoalCT_model->getLastID();
                            $dataKategoriLevel = $this->KategoriLevelSoalCT_model->insert_kategori_level_soal($idSoal,$item['kategori_umur'],$item['level_soal']);
                        }

                        
                    //insert item gambar dan tipe gambar (Bagian Soal)
                    
                    if(!empty($_FILES['multipleFiles']['tmp_name'])){
                            
                            for($i = 0 ; $i < count($_FILES['multipleFiles']['tmp_name']); $i++){
                                $data3[] = array('multipleFiles' => $_FILES['multipleFiles']['tmp_name'][$i],
                                                'tipe_gambar' => $_POST['tipe_gambar'][$i],
                                                'keteranganGambar'=> $_POST['keteranganGambar'][$i],
                                                'ukuran_file' => $_FILES['multipleFiles']['size'][$i]);
                            }
                            //print_r($data3);
                        

                        foreach($data3 as $item){
                            $idSoal = $this->SoalCT_model->getLastID();
                            $image = $item['multipleFiles'];
                            $tipe_gambar = $item['tipe_gambar'];
                            $keteranganGambar = $item['keteranganGambar'];
                            $ukuran_file = $item['ukuran_file'];

                            //echo "<br> ukuran file : ".$ukuran_file;
                            

                            if($image != null){
                                $image = file_get_contents( $image );
                                $image = base64_encode($image);
                            }
                            else{
                                $image = null;
                            }
                            
                            if($image != null){
                                $dataGambar = $this->UploadGambar_model->insert_gambar($image,$tipe_gambar,$keteranganGambar);
                                $idGambar = $this->UploadGambar_model->getLastImageID();
                                $dataSoalGambar = $this->ImageSoalCT_model->insert_image_soal($idSoal,$idGambar); 
                            }


                        }
                    }
                        
                
                    //insert item gambar dan tipe gambar (Bagian Pembahasan)

                    if(!empty($_FILES['multiplePembahasan']['tmp_name'])){
                        for($i = 0 ; $i < count($_FILES['multiplePembahasan']['tmp_name']); $i++){
                            $data4[] = array('multiplePembahasan' => $_FILES['multiplePembahasan']['tmp_name'][$i],
                                            'tipe_gambarPembahasan' => $_POST['tipe_gambarPembahasan'][$i],
                                            'keteranganGambarPembahasan' => $_POST['keteranganGambarPembahasan'][$i]);
                        }
                        //print_r($data3);
                    

                    foreach($data4 as $item){
                        $idSoal = $this->SoalCT_model->getLastID();
                        $image = $item['multiplePembahasan'];
                        $tipe_gambar = $item['tipe_gambarPembahasan'];
                        $keteranganGambarPembahasan = $item['keteranganGambarPembahasan'];
                        
                        if($image != null){
                            $image = file_get_contents( $image );
                            $image = base64_encode($image);
                        }
                        else{
                            $image = null;
                        }
                        
                        $setAutoIncrement = $this->UploadGambar_model->resetAI();
                        if($image != null){
                            $dataGambar = $this->UploadGambar_model->insert_gambar($image,$tipe_gambar,$keteranganGambarPembahasan);
                            $idGambar = $this->UploadGambar_model->getLastImageID();
                            $dataPembahasanGambar = $this->ImagePembahasanCT_model->insert_image_pembahasan($idSoal,$idGambar); 
                        }
                        //echo $item['tipe_gambar'];
                        }


                    }   
                    
                    $data['success'] = ($dataSoalCT && $dataPembahasanSoalCT);
                    $this->session->unset_userdata('gagal_file_terlalu_besar');
                    $this->session->unset_userdata('gagal_tipe_file_salah');
                    $this->session->set_flashdata('success','Sukses : Berhasil disimpan');
                    $this->load->view("admin/data_soal/new_multi_form",$data);
                
                }
                else{
                    $this->session->unset_userdata('success');
                    $this->session->unset_userdata('gagal_tipe_file_salah');
                    $this->session-> set_flashdata('gagal_file_terlalu_besar', 'Gagal : Ukuran gambar terlalu besar, mohon pastikan ukuran gambar untuk soal CT dan pembahasan dibawah 2 MB!'); 
                    $this->load->view("admin/data_soal/new_multi_form",$data);
                }
              }
              else{
                $this->session->unset_userdata('success');
                $this->session->unset_userdata('gagal_file_terlalu_besar');
                $this->session-> set_flashdata('gagal_tipe_file_salah', 'Gagal : Tipe file tidak sesuai, mohon pastikan tipe gambar untuk soal CT dan pembahasan adalah jpeg/png/gif !'); 
                $this->load->view("admin/data_soal/new_multi_form",$data);
              }

            }
            else{     
                $this->session->unset_userdata('success');
                $this->session->unset_userdata('gagal_file_terlalu_besar');
                $this->session-> set_flashdata('gagal_tipe_file_salah', 'Gagal : Tipe file tidak sesuai, mohon pastikan tipe gambar untuk soal CT dan pembahasan adalah jpeg/png/gif !'); 
                $this->load->view("admin/data_soal/new_multi_form",$data);
            }



        }else{
            $this->session->unset_userdata('success');
            $this->session->unset_userdata('gagal_tipe_file_salah');
            $this->session->unset_userdata('gagal_file_terlalu_besar');
            $this->load->view("admin/data_soal/new_multi_form",$data);
        }


    }

    public function edit($id = null){
                                                                            //$id = id data yang akan diedit, nilai awal null

        if(!isset($id)){
            redirect("admin/soal_ct");                                      //melakuan redirect jika tidak ada id

        }

        if($this->SoalCT_model->validate_form() === TRUE){                                //menjalankan validasi
            $this->SoalCT_model->update();                                                //menyimpan data ke database
        }

        $data["soal_ct"] = $this->SoalCT_model->getById($id);                          //mengambil data untuk ditampilkan pada form
        $data["image_ct"] = $this->UploadGambar_model->get_imageSoal($id);
        $data["pembahasan_soal_ct"] = $this->PembahasanSoalCT_model->getByID($id);
        $data["pembahasan_judul_soal"] = $this->PembahasanSoalCT_model->getJudulSoalByID($id);
        $data["image_pembahasan"] = $this->UploadGambar_model->get_imagePembahasan($id);    
        $data['konsep_ct'] = $this->KonsepCT_model->getAll();
        $data['id_konsep_ct']=$this->KonsepCT_Soal_model->get_idKonsepCT_by_idSoalCT($id);
        $data['id_tag'] = $this->TagCT_soal_model->get_idTag_by_idSoalCT($id);
        $data['tag_ct'] = $this->TagCT_model->getAll();
        $data['jumlah_tag'] = $this->TagCT_model->jumlah_data_tag();
        $data['kategori_level_soal'] = $this->KategoriLevelSoalCT_model->get_kategoriLevelSoal($id);
        $data['kategori_umur'] = $this->KategoriUmurCT_model->getAll();
        $data['level_soal'] = $this->LevelSoalCT_model->getAll();
        $data['negara_pembuat_soal'] = $this->Negara_model->getAll();
        
        if(!$data["soal_ct"]){                                               //jika tida ada, tampilkan error 404
           show_404();
        }
        $this->load->view("admin/data_soal/edit_form_soal",$data);          //menampilkan form edit soal
    }



    public function delete($id = null){
        //menghapus data soal
        //id data yang akan delete, nilai awal null
        //apabila data berhasil dihapus di alihkan/redirect ke halaman list soal

        if(!isset($id)){
            show_404(); //melakuan redirect jika tidak ada id
        }

        $this->UploadGambar_model->delete_imageSoal($id);
        $this->UploadGambar_model->delete_imagePembahasan($id);
        $this->SoalCT_model->delete($id);
        $this->PembahasanSoalCT_model->delete($id);
        $this->KonsepCT_Soal_model->delete_konsep_soal($id);
        $this->TagCT_Soal_model->delete_tag_soal($id);
        $this->KategoriLevelSoalCT_model->delete($id);
        
        $this->index();
        redirect('admin/soal_ct');
    }

    public function new_form_pembahasan(){
        $this->load->view('admin/data_soal/new_form_pembahasan');
    }


    public function tampilkanDataSoal(){
        $id=$this->uri->segment(4);
		$data["soal_ct"] = $this->SoalCT_model->getAll();
		$data['data_soal'] = $this->SoalCT_model->per_id($id);
		$data['data_pembahasan'] = $this->PembahasanSoalCT_model->per_id($id);
		$data['data_imageSoal'] = $this->UploadGambar_model->get_id_imageSoalDeskripsi($id);
		$data['data_imagePertanyaan']  = $this->UploadGambar_model->get_id_imageSoalPertanyaan($id);
		$data['data_imageJawaban'] = $this->UploadGambar_model->get_id_imagePembahasanJawaban($id);
		$data['data_imagePenjelasan'] = $this->UploadGambar_model->get_id_imagePembahasanPenjelasan($id);
		$data['data_tag'] = $this->TagCT_soal_model->getTag_by_idSoalCT($id);
		$data['data_konsepCT'] = $this->KonsepCT_Soal_model->getKonsepCT_by_idSoalCT($id);
		$data['data_kategori_umur'] = $this->SoalCT_model->get_kategori_umur();
        $data['data_negara'] = $this->SoalCT_model->get_negara_soal($id);
        
		$this->load->view('admin/data_soal/soal_dan_pembahasan',$data);
    }

    function display_gambar($id_gambar=null){
        return $this->UploadGambar_model->display_gambar($id_gambar);
    }
    
    
    function delete_image($id_image){
        $id_soal_ct = $this->UploadGambar_model->delete_image($id_image);
        $this->SoalCT_model->update(); 
        redirect('admin/soal_ct/edit/'.$id_soal_ct); 
    }


    function delete_image_pembahasan($id_image){
        $id_soal_ct = $this->UploadGambar_model->delete_image_pembahasan($id_image);
        $this->SoalCT_model->update(); 
        redirect('admin/soal_ct/edit/'.$id_soal_ct);     
    }
    

    function delete_kategori_level_soal($id_soal_ct,$id_kategori_umur,$id_level_soal){
        
        $id_soal = $this->KategoriLevelSoalCT_model->delete_kategori_level_soal($id_soal_ct,$id_kategori_umur,$id_level_soal);
        //$this->SoalCT_model->update();
        $this->edit($id_soal);
        redirect('admin/soal_ct/edit/'.$id_soal);
          
    }

}