<?php
defined('BASEPATH') OR exit('No direct script allowed');

class Kategori_Umur_CT extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("KategoriUmurCT_model"); 
        $this->load->model("Negara_model");
        $this->load->library('form_validation'); 
        $this->load->library('pagination');             // meload library pagination
    }

    public function index(){
        $data["kategori_umur_ct"] = $this->KategoriUmurCT_model->getAll();

        $config['base_url'] = base_url("index.php/admin/kategori_umur_ct/index/");
        $config['total_rows'] = $this->KategoriUmurCT_model->jumlah_data_kategoriUmur();
        $config['per_page'] = $this->KategoriUmurCT_model->jumlah_data_kategoriUmur();
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        

        $this->pagination->initialize($config);	
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["data_kategori_umur"] = $this->KategoriUmurCT_model->get_data_kategoriUmur($config['per_page'],$data['page']);
		$data['pagination'] = $this->pagination->create_links();

        $this->load->view("admin\data_properti_ct\data_kategori_umur\list_kategori_umur", $data);

    }

    public function add(){


        $kategori_umur = $this->KategoriUmurCT_model; 
        $validation = $kategori_umur->validate_form();  

        if($validation == TRUE){           
            if($kategori_umur->save()){
                $this->session->set_flashdata('success','Berhasil disimpan'); 
                redirect('admin/kategori_umur_ct/add'); 

            }
            else{
                $this->session->set_flashdata('gagal_kategori_umur', 'Terjadi kesalahan : Nama kategori umur, umur awal, dan umur akhir tidak boleh sama!');
            }
        }

        $data['negara'] = $this->Negara_model->getAll(); 
        $this->load->view("admin/data_properti_ct/data_kategori_umur/new_form_kategori_umur",$data); 

    }

    public function edit($id = null){

        
        if(!isset($id)){
            redirect('admin/kategori_umur_ct'); 
        }

        $kategori_umur = $this->KategoriUmurCT_model; 
        $validation = $kategori_umur->validate_form(); 



        if($validation == TRUE){ 
           
            if($kategori_umur->update() == true){
                $this->session->set_flashdata('success','Berhasil disimpan');
                redirect('admin/kategori_umur_ct/edit/'.$id); 
            }
            else{
                $this->session->set_flashdata('gagal_kategori_umur', 'Terjadi kesalahan : Nama kategori umur, umur awal, dan umur akhir tidak boleh sama!');
            }
        }
        

        $data["kategori_umur_id"] = $kategori_umur->getById($id); 
        $data['negara'] = $this->Negara_model->getAll();

        if(!$data["kategori_umur_id"]){                   
            show_404();
        }



        $this->load->view("admin/data_properti_ct/data_kategori_umur/edit_form_kategori_umur",$data); 
    }


    public function delete($id){
        if(!isset($id)){
            show_404();
        }

        $kategori_umur = $this->KategoriUmurCT_model; 

        if($kategori_umur->delete($id)){
            redirect('admin/kategori_umur_ct');
        }
    }

    

   

}