<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LiputanCT_model extends CI_Model{

    private $_table = "master_liputan_ct";
    private $id_liputan;
    private $judul_liputan;
    private $deskripsi_liputan;
    private $tanggal;

    public function __construct(){
        parent::__construct();
        $this->load->model("UploadGambar_model");
        $this->load->model("ImageLiputanCT_model");
        $this->load->library("form_validation"); //meload library form validation
    }

    public function getAll(){
        $query = $this->db->query("select * from master_liputan_ct");
        $res = $query->result();
        return $res;
        //return $this->db->get($this->_table)->result();
         //Artinya seperti select * from master_liputan_ct
    }

    public function getAll_desc(){
        $query = $this->db->query("select * from master_liputan_ct order by id_liputan desc");
        $res = $query->result();
        return $res;
        //return $this->db->get($this->_table)->result();
         //Artinya seperti select * from master_liputan_ct
    }

    function validate_form(){

        $res=false;

        $maxsize = 2097152;
        $max_img_liputan = 0;
        $max_img_liputan2 = 0;
        
        //arr untuk menampung size
        $arr_edit_liputan = [];
        $arr_tambah_liputan = [];

        //arr untuk menampung type
        $arr_editLiputan_type = [];
        $arr_tambahLiputan_type = [];

        $cek_editLiputan_type = "true";
        $cek_tambahLiputan_type = "true";
        
        if($this->input->post()){
            $this->form_validation->set_rules('judul_liputan','Judul Liputan','required');
            $this->form_validation->set_rules('deskripsi_liputan','Deskripsi Liputan', 'required');
            $this->form_validation->set_rules('tanggal','Tanggal', 'required');
            
            $validation = $this->form_validation->run();

            //validasi edit gambar soal
            if(!empty($_FILES['multipleFiles']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multipleFiles']['size']); $i++){
                    $arr_edit_liputan[$i] = $_FILES['multipleFiles']['size'][$i];
                    if($arr_edit_liputan[$i] > $max_img_liputan){
                        $max_img_liputan = $arr_edit_liputan[$i]; 
                    }

                    $arr_editLiputan_type[$i] = $_FILES['multipleFiles']['type'][$i];
                    if($arr_editLiputan_type[$i] == "image/jpeg" || $arr_editLiputan_type[$i] == "image/gif" || $arr_editLiputan_type[$i] == "image/png" || empty($arr_editLiputan_type[$i])){
                        $cek_editLiputan_type = "true";
                    }
                    else{
                        $cek_editLiputan_type = "false";
                        break;
                    }
                }

              //  print_r($_FILES['multipleFiles']['size']);
            }

             //validasi tambah gambar liputan
             if(!empty($_FILES['multipleFiles2']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multipleFiles2']['size']); $i++){
                    $arr_tambah_liputan[$i] = $_FILES['multipleFiles2']['size'][$i];
                    if($arr_tambah_liputan[$i] > $max_img_liputan2){
                        $max_img_liputan2 = $arr_tambah_liputan[$i]; 
                    }

                    $arr_tambahLiputan_type[$i] = $_FILES['multipleFiles2']['type'][$i];
                    if($arr_tambahLiputan_type[$i] == "image/jpeg" || $arr_tambahLiputan_type[$i] == "image/gif" || $arr_tambahLiputan_type[$i] == "image/png" || empty($arr_tambahLiputan_type[$i])){
                        $cek_tambahLiputan_type = "true";
                    }
                    else{
                        $cek_tambahLiputan_type = "false";
                        break;
                    }
                }

               // print_r($_FILES['multipleFiles2']['size']);
            }


            if( $this->form_validation->run() == TRUE){

                if($cek_editLiputan_type == "true" && $cek_tambahLiputan_type == "true"){
    
                    if($max_img_liputan <= $maxsize && $max_img_liputan2 <= $maxsize){
                        if($this->session->has_userdata('success') == FALSE){
                            $this->session->sess_destroy();
                            $this->session->set_flashdata('success', 'Sukses : Berhasil disimpan');
                        }
                        $res = TRUE; 
                    }
                    else{                                  
                        if($this->session->has_userdata('gagal_file_terlalu_besar') == FALSE){
                            $this->session->sess_destroy();
                            $this->session->set_flashdata('gagal_file_terlalu_besar', 'Gagal : Ukuran gambar terlalu besar, mohon pastikan ukuran gambar untuk Liputan CT dibawah 2 MB!');                          
                        }                    
                    }
                }
                else{
                    if($this->session->has_userdata('gagal_tipe_file_salah') == FALSE){
                        $this->session->sess_destroy();
                        $this->session-> set_flashdata('gagal_tipe_file_salah', 'Gagal : Tipe file tidak sesuai, mohon pastikan tipe gambar untuk Liputan CT adalah jpeg/png/gif !');  
                    }                
                }
            }
            else{
                $this->session->sess_destroy();          
            }


        }

        return $res;
    }

    function get_liputan_by_id($id){
     $query = $this->db->query("select id_liputan, 
                                        judul_liputan,
                                        deskripsi_liputan,
                                        tanggal
                                from master_liputan_ct
                                where id_liputan = $id
     ");
    
     $res = $query->row();
     return $res;
    }

    function per_id($id) {
        $this->db->where('id_soal_ct',$id);
        $query=$this->db->get('master_soal_ct');
        return $query->result();
    }

    public function getLastID(){
        $query = $this->db->query("SELECT id_liputan FROM master_liputan_ct ORDER BY id_liputan DESC LIMIT 1");
        $id = $query->row()->id_liputan;
        return $id;
    }

    function save(){
        $post = $this->input->post(); 
        $this->judul_liputan = $post["judul_liputan"];
        $this->deskripsi_liputan = $post["deskripsi_liputan"];
        $this->tanggal = $post["tanggal"];

        $sql = "INSERT INTO master_liputan_ct (judul_liputan,deskripsi_liputan,tanggal)
        VALUES (".$this->db->escape($this->judul_liputan).",'$this->deskripsi_liputan','$this->tanggal');";

        

        $this->db->query("$sql");
    
        //insert gambar liputan
        if(!empty($_FILES['multipleFiles']['tmp_name'])){
                            
            for($i = 0 ; $i < count($_FILES['multipleFiles']['tmp_name']); $i++){
                $data3[] = array('multipleFiles' => $_FILES['multipleFiles']['tmp_name'][$i],
                                'keteranganGambar'=> $_POST['keteranganGambar'][$i],
                                'ukuran_file' => $_FILES['multipleFiles']['size'][$i]);
            }
    
            foreach($data3 as $item){
                $id_liputan = $this->getLastID();
                $image = $item['multipleFiles'];
                $keteranganGambar = $item['keteranganGambar'];
                $ukuran_file = $item['ukuran_file'];
    
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                }
                else{
                    $image = null;
                }
                
                if($image != null){
                    $dataGambar = $this->UploadGambar_model->insert_gambar_liputan($image,$keteranganGambar);
                    $idGambar = $this->UploadGambar_model->getLastImageLiputanID();
                    $dataLiputanGambar = $this->ImageLiputanCT_model->insert_image_liputan($id_liputan,$idGambar); 
                }
            }
        }

        $this->db->query('alter table master_liputan_ct AUTO_INCREMENT=0');
    }

    function update(){
        $post = $this->input->post();
        $this->id_liputan = $post["id_liputan"];
        $this->judul_liputan = $post["judul_liputan"];
        $this->deskripsi_liputan = $post["deskripsi_liputan"];
        $this->tanggal = $post["tanggal"];

        //Edit gambar liputan pada edit liputan form
        if(!empty($_FILES['multipleFiles']['tmp_name'])){
            for($i = 0 ; $i < count($_FILES['multipleFiles']['tmp_name']); $i++){
                $dataGambarSoal[] = array('id_image'=>$post['id_image'][$i],
                                        'multipleFiles' => $_FILES['multipleFiles']['tmp_name'][$i],
                                        'keteranganGambar' => $post['keteranganGambar'][$i]);
            }
            
            foreach($dataGambarSoal as $item){
                $idLiputan = $this->id_liputan;
                $id_image = $item['id_image'];
                $image = $item['multipleFiles'];
                $keteranganGambar = $item['keteranganGambar'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                    $updateLiputanGambar = $this->UploadGambar_model->update_image_liputan($id_image,$image,$keteranganGambar); 
                }
                else{
                    $image = null;
                }
                
            }
        
        }

        //Tambah gambar soal pada edit soal form

        if(!empty($_FILES['multipleFiles2']['tmp_name'])){
            
            for($i = 0 ; $i < count($_FILES['multipleFiles2']['tmp_name']); $i++){
                $data1[] = array('multipleFiles2' => $_FILES['multipleFiles2']['tmp_name'][$i],
                            'keteranganGambar2' => $_POST['keteranganGambar2'][$i]);
            }

            foreach($data1 as $item){
                $idLiputan =  $this->id_liputan;
                $image = $item['multipleFiles2'];
                $keteranganGambar = $item['keteranganGambar2'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                }
                else{
                    $image = null;
                }

                if($image != null){
                    $dataGambar = $this->UploadGambar_model->insert_gambar_liputan($image,$keteranganGambar);
                    $idGambar = $this->UploadGambar_model->getLastImageLiputanID();
                    $dataLiputanGambar = $this->ImageLiputanCT_model->insert_image_liputan($idLiputan,$idGambar);
                }
            }
        }

        $sql = "UPDATE master_liputan_ct
                SET judul_liputan = ".$this->db->escape($this->judul_liputan)." ,
                    deskripsi_liputan = '$this->deskripsi_liputan',
                    tanggal = '$this->tanggal'
                WHERE id_liputan = '$this->id_liputan';";

        $this->db->query("$sql");
    }

    function delete($id){
        $this->db->query("DELETE FROM master_liputan_ct WHERE id_liputan = '$id'");
        $this->db->query('alter table master_liputan_ct AUTO_INCREMENT=0');
    }

    function jumlah_data_liputan(){
		return $this->db->get('master_liputan_ct')->num_rows();
    }
    
    function get_data_liputan($number,$offset){
        $this->db->order_by("id_liputan desc");
		return $query = $this->db->get('master_liputan_ct',$number,$offset)->result();		
	}

}