<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ImageSoalCT_model extends CI_Model{

    private $_table = "image_soal";

    function insert_image_soal ($id_soal_ct,$id_image){
        $data = array('id_soal_ct' => $id_soal_ct, 'id_image' => $id_image);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;
    }

}