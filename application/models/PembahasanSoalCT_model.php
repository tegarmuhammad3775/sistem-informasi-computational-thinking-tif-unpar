<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class PembahasanSoalCT_model extends CI_Model{

    private $_table="master_pembahasan_soal_ct";
    private $id_pembahasan;
    private $jawaban;
    private $penjelasan;

    public function __construct(){
        parent::__construct();
        $this->load->model("UploadGambar_model");
        $this->load->model('ImagePembahasanCT_model');
        $this->load->library("form_validation"); //meload library form validation
    }


    public function rules(){
        return[
            
            [ 'field'  => 'jawaban',
               'label' => 'Jawaban',
               'rules' => 'required' ],
            [ 'field'  => 'penjelasan',
               'label' => 'Penjelasan',
               'rules' => 'required' ],
        ];

    }
    
    public function getAll(){
        return $this->db->get($this->_table)->result();
        //Artinya seperti select * from master_pembahasan_soal_ct
    }

    public function getAllJoin(){
        $this->db->join('master_soal_ct', 'master_soal_ct.id_pembahasan = master_pembahasan_Soal_ct.id_pembahasan', 'inner'); 
        $query = $this->db->get('master_pembahasan_soal_ct');
        return $query->result();
    }

    public function getLeftOuterJoin(){
        $this->db->select('master_soal_ct.judul_soal');
        $this->db->from('master_soal_ct');
        $this->db->join('master_pembahasan_soal_ct', 'master_soal_ct.id_soal_ct = master_pembahasan_soal_ct.id_pembahasan');
        $this->db->where('master_soal_ct.judul_soal is NULL', NULL, FALSE);
        $query = $this->db->get();
        return $query->result();
    }

    public function getJudulSoalByID($id){
        $this->db->select('judul_soal');
        $this->db->from('master_soal_ct');
        $this->db->where('id_soal_ct', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function getByID($id){
        $query = $this->db->query("
                                    SELECT *
                                    FROM 
                                        master_pembahasan_soal_ct
                                    WHERE master_pembahasan_soal_ct.id_soal_ct = $id;
        ");

        $query = $this->db->escape($query);
        return $query->row();    
    }

    function per_id($id) {
        $this->db->join('master_soal_ct', 'master_pembahasan_soal_ct.id_soal_ct= master_soal_ct.id_soal_ct','rigt');
        $this->db->where('master_soal_ct.id_soal_ct',$id);
        $query=$this->db->get('master_pembahasan_soal_ct');
        return $query->result();
    }

    public function save(){

        $temp = $this->db->insert_id();
        $post = $this->input->post();                                //mengambil data dari form
        $this->id_pembahasan = $temp;
        $this->jawaban = $post["jawaban"];                       //data jawaban dari form
        $this->penjelasan = $post["penjelasan"];                 //data penjelasan dari form
        $this->penjelasan_ct = $post["penjelasan_ct"];
        $this->db->insert($this->_table,$this);                  //melakukan insert data pada tabel master_pembahasan_soal_ct
        $this->db->query('call generate_id_pembahasan()');
        $this->db->query('alter table master_pembahasan_soal_ct AUTO_INCREMENT 0');
    }


    public function update(){
        $post = $this->input->post();                            //mengambil data dari form
        $this->id_pembahasan = $post['id_pembahasan'];           //data id_pembahasan dari form
        $this->jawaban = $post['jawaban'];                       //data jawaban dari form
        $this->penjelasan = $post['penjelasan'];                 //data penjelasan dari form
        $this->penjelasan_ct = $post['penjelasan_ct'];                 //data penjelasan dari form



        //Tambah soal pembahasan baru

        if(!empty($_FILES['multiplePembahasan']['tmp_name'])){
        
            for($i = 0 ; $i < count($_FILES['multiplePembahasan']['tmp_name']); $i++){
                $data4[] = array('multiplePembahasan' => $_FILES['multiplePembahasan']['tmp_name'][$i],'tipe_gambarPembahasan' => $_POST['tipe_gambarPembahasan'][$i]);
            }
    
            foreach($data4 as $item){
               // $idSoal = $this->SoalCT_model->getLastID();
                $idPembahasan =  $this->id_pembahasan;
                $image = $item['multiplePembahasan'];
                $tipe_gambar = $item['tipe_gambarPembahasan'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                }
                else{
                    $image = null;
                }
                
                $setAutoIncrement = $this->UploadGambar_model->resetAI();
    
                if($image != null){
                    $dataGambar = $this->UploadGambar_model->insert_gambar($image,$tipe_gambar);
                    $idGambar = $this->UploadGambar_model->getLastImageID();
                    $dataSoalGambar = $this->ImagePembahasanCT_model->insert_image_pembahasan($idPembahasan,$idGambar);
                }
            }
           }


        
        //Edit soal pembahasan pada edit soal form

        if(!empty($_FILES['multipleFiles']['tmp_name'])){
            for($i = 0 ; $i < count($_FILES['multipleFiles']['tmp_name']); $i++){
             $data5[] = array('id_image'=>$post['id_image'][$i],'multipleFiles' => $_FILES['multipleFiles']['tmp_name'][$i],'tipe_gambar' => $post['tipe_gambar'][$i]);
          }
         }
 
                 foreach($data5 as $item){
                     $idSoal = $this->SoalCT_model->getLastID();
                     $id_image = $item['id_image'];
                     $image = $item['multipleFiles'];
                     $tipe_gambar = $item['tipe_gambar'];
                     
                     if($image != null){
                         $image = file_get_contents( $image );
                         $image = base64_encode($image);
                         $updateSoalGambar = $this->UploadGambar_model->update_image_soal($id_image,$image,$tipe_gambar); 
                     
                     }
                     else{
                         $image = null;
                     }
         
                     
                 }
        $this->db->update($this->_table,$this,array('id_pembahasan'=>$post['id_pembahasan']));    //melakukan update data pada tabel master_pembahasan_soal_ct

    }

    public function delete($id){
        //method ini menghapus data dengan id_pembahasan = $id pada tabel master_pembahasan_soal_ct     
        $this->db->delete($this->_table,array('id_pembahasan'=>$id));
        $this->db->query('alter table master_pembahasan_soal_ct AUTO_INCREMENT 0');


    }

    public function insert_pembahasan_soal_ct($id_soal_ct,$jawaban,$penjelasan,$penjelasan_ct){
        $data = array('id_soal_ct'=>$id_soal_ct,'jawaban'=>$jawaban,'penjelasan'=>$penjelasan, 'penjelasan_ct'=>$penjelasan_ct);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
           // $this->db->query('call generate_id_pembahasan()');
            return TRUE;       
        }
        return FALSE;

    }

    public function update_pembahasan_soal_ct($idPembahasan,$jawaban,$penjelasan,$penjelasan_ct){
        $query = $this->db->query("
                                    UPDATE master_pembahasan_soal_ct
                                    SET master_pembahasan_soal_ct.jawaban = ".$this->db->escape($jawaban).",
                                    master_pembahasan_soal_ct.penjelasan = ".$this->db->escape($penjelasan).",
                                    master_pembahasan_soal_ct.penjelasan_ct = ".$this->db->escape($penjelasan_ct)."
                                     WHERE master_pembahasan_soal_ct.id_pembahasan = '$idPembahasan';
        ");

    }

    public function getLastID(){
        $query = $this->db->query("SELECT id_pembahasan FROM master_pembahasan_soal_ct ORDER BY id_pembahasan DESC LIMIT 1");
        $id = $query->row()->id_pembahasan;
        return $id;
    }


}