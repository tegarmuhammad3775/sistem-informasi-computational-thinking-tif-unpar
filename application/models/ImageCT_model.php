<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ImageCT_model extends CI_Model{

    function __construct(){
        $this->tableName = 'master_image_ct';

    }

    public function getRows($id=''){
        $this->db->select('image_content');
        $this->db->from('master_image_ct');
        if($id){
            $this->db->where('id_image',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }
        else{
            $query = $this->db->get();
            $result = $query->result_array();
        }

        return !empty($result)?$result:false;

    }

    public function insert($data=array()){
        $insert = $this->db->insert_batch('master_image_ct',$data);
        return $insert?true:false;

    }

}