<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KonsepCT_model extends CI_Model{

    private $_table = "master_konsep_ct";

    public function getAll(){
        return $this->db->get($this->_table)->result();
          //Artinya seperti select * from master_level_soal
     }

    public function getById($id){
      $query = $this->db->query("
                                  SELECT *
                                  from master_konsep_ct
                                  where master_konsep_ct.id_konsep_ct = $id;
      ");
      $res = $query->row();
      return $res;
    }

    public function validate_form(){
      $this->form_validation->set_rules('konsep_ct','Konsep CT','required');
      
      $validation = $this->form_validation->run();
      if($validation == TRUE){
        return TRUE;
      }
      else{
        return FALSE;
      }

    }

    public function save(){

      $post = $this->input->post(); 
      $konsep_ct = $post["konsep_ct"];

      $data_konsep = $this->getAll();
      $cek = true;

      $this->db->query('alter table master_konsep_ct AUTO_INCREMENT=0');

      foreach($data_konsep as $i){
        $konsep_temp = $i->konsep_ct;
        
        if($konsep_ct == $konsep_temp){
          $cek = false;
          break;
        }

      }


      if($cek == true){
          $query = $this->db->query("
                                    INSERT INTO master_konsep_ct(konsep_ct)
                                    VALUES ('$konsep_ct');
          ");
        return true;
      }
      else{
        return false;
      }
  }

    public function update(){
        $post = $this->input->post();
        $id_konsep_ct = $post["id_konsep_ct"];
        $konsep_ct = $post["konsep_ct"];

        $data_konsep = $this->getAll();
        $cek = true;

        foreach($data_konsep as $i){
          $id_temp = $i->id_konsep_ct;
          $konsep_temp = $i->konsep_ct;
          
          if($id_konsep_ct != $id_temp && $konsep_ct == $konsep_temp){
            $cek = false;
            break;
          }
  
        }

        if($cek == true){
          $query = $this->db->query("
            UPDATE master_konsep_ct
            SET konsep_ct = ".$this->db->escape($konsep_ct)."
            WHERE id_konsep_ct = $id_konsep_ct
          "); 
          return true;
        }
        else{
          return false;
        }   

    }

    public function delete($id){
      //method ini menghapus data pada tabel berdasarkan id
      return $this->db->delete($this->_table,array("id_konsep_ct" => $id));
    }

    function jumlah_data_konsep(){
      return $this->db->get('master_konsep_ct')->num_rows();
    }
      
    function get_data_konsep($number,$offset){
      $this->db->order_by("id_konsep_ct desc");
      return $query = $this->db->get('master_konsep_ct',$number,$offset)->result();		
    }


}