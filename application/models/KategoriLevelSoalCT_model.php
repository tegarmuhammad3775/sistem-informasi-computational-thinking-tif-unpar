<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriLevelSoalCT_model extends CI_Model{
    private $_table="kategori_level_soal";

    function insert_kategori_level_soal ($id_soal_ct,$id_kategori_umur,$id_level_soal){
        $data = array('id_soal_ct'=> $id_soal_ct,'id_kategori_umur' => $id_kategori_umur, 'id_level_soal' => $id_level_soal);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;    
    }

    function get_kategoriLevelSoal($id_soal_ct){
        $query = $this->db->query("
                                    SELECT kategori_level_soal.id_soal_ct,
                                           master_kategori_umur.id_kategori_umur,
                                           master_kategori_umur.nama_kategori,
                                           master_kategori_umur.umur_awal,
                                           master_kategori_umur.umur_akhir,
                                           master_negara.negara_singkatan,
                                           master_level_soal.id_level_soal, 
                                           master_level_soal.level_soal, 
                                           master_level_soal.keterangan 
                                    FROM master_kategori_umur 
                                    JOIN master_negara
                                    on master_kategori_umur.id_negara = master_negara.id_negara 
                                    JOIN kategori_level_soal
                                    on master_kategori_umur.id_kategori_umur = kategori_level_soal.id_kategori_umur 
                                    JOIN master_level_soal 
                                    on master_level_soal.id_level_soal = kategori_level_soal.id_level_soal
                                    WHERE kategori_level_soal.id_soal_ct = '$id_soal_ct'
                                    ORDER by kategori_level_soal.id_kategori_umur;
        ");
        $res = $query->result();
        if($res != null){
            return $res;
        }else{
            $res = false;
            return $res;
        }

    }

    function delete_kategori_level_soal($id_soal_ct,$id_kategori_umur,$id_level_soal){
        $query = $this->db->query("
                                    SELECT kategori_level_soal.id_soal_ct 
                                    FROM kategori_level_soal
                                    WHERE kategori_level_soal.id_soal_ct = $id_soal_ct AND
                                          kategori_level_soal.id_kategori_umur = $id_kategori_umur AND
                                          kategori_level_soal.id_level_soal = $id_level_soal;
        ");
        $res = $query->row()->id_soal_ct;
        
        $this->db->query("
                            DELETE kategori_level_soal
                            FROM kategori_level_soal
                            WHERE kategori_level_soal.id_soal_ct = $id_soal_ct AND
                                  kategori_level_soal.id_kategori_umur = $id_kategori_umur AND
                                  kategori_level_soal.id_level_soal = $id_level_soal;
        ");

        return $res;
    }

    public function delete($id_soal_ct){
        $query = $this->db->query("
                                    DELETE FROM kategori_level_soal
                                    WHERE kategori_level_soal.id_soal_ct = '$id_soal_ct';
        ");
    }


}