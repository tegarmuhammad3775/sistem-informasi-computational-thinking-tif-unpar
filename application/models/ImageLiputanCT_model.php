<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ImageLiputanCT_model extends CI_Model{

    private $_table = "image_liputan";

    function insert_image_liputan ($id_liputan,$id_image){
        $data = array('id_liputan' => $id_liputan, 'id_image' => $id_image);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;
    }

}